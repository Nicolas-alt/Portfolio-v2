import * as THREE from 'three'
import matcapImage from '../../assets/matcaps/8.jpg'

/**
 * =============================================
 *
 *  SCENE & CAMERA
 *
 * =============================================
 */
const sizes = {
  height: window.innerHeight,
  width: window.innerWidth
}

const scene = new THREE.Scene()

const light = new THREE.AmbientLight(0xffffff)
scene.add(light)

const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
)

camera.position.x = 5
camera.rotation.y = 1.5

scene.add(camera)

const renderer = new THREE.WebGLRenderer({
  alpha: true,
  canvas: document.querySelector('.webgl')
})
renderer.setPixelRatio(window.devicePixelRatio)
renderer.setSize(sizes.width, sizes.height)

/**
 * =============================================
 *
 *  CUSTOM FUNCTIONS && TEXTURE LOADER
 *
 * =============================================
 */

const textureLoader = new THREE.TextureLoader()
const matcapTexture = textureLoader.load(matcapImage)

/**
 * Function for make a new little star
 * @returns New white star
 */
const createStar = () => {
  const geometry = new THREE.SphereGeometry(0.25, 24, 24)
  const material = new THREE.MeshStandardMaterial({ color: 0x728af4 })
  const star = new THREE.Mesh(geometry, material)

  const [x, y, z] = Array(3)
    .fill()
    .map(() => THREE.MathUtils.randFloatSpread(100))

  star.position.set(x, y, z)
  scene.add(star)
}

Array(200).fill().forEach(createStar)

const createTorusTriangle = (
  radius = 10,
  tube = 3,
  rSegmets = 16,
  tubularSegments = 3,
  x = -3,
  y = 2,
  z = -8
) => {
  const geometry2 = new THREE.TorusGeometry(
    radius,
    tube,
    rSegmets,
    tubularSegments
  )
  const material = new THREE.MeshMatcapMaterial()
  material.matcap = matcapTexture

  const sizeShape = 0.08

  const meshGeometry = new THREE.Mesh(geometry2, material)
  meshGeometry.position.x = x
  meshGeometry.position.y = y
  meshGeometry.position.z = z
  meshGeometry.rotateZ(8)
  meshGeometry.scale.x = sizeShape
  meshGeometry.scale.y = sizeShape
  meshGeometry.scale.z = sizeShape
  scene.add(meshGeometry)

  return meshGeometry
}

/**
 *
 * @param {number} x Position in x?= -1
 * @param {number} y Position in y?= -3
 * @param {number} z Position in z?= 5
 * @returns Three icosahedron geometry
 */

const createIcosahedronGeometry = (x = -1, y = -3, z = 5) => {
  const geometry2 = new THREE.IcosahedronGeometry()
  const material = new THREE.MeshMatcapMaterial()
  material.matcap = matcapTexture

  const sizeShape = 0.08

  const meshGeometry = new THREE.Mesh(geometry2, material)
  meshGeometry.position.x = x
  meshGeometry.position.y = y
  meshGeometry.position.z = z
  meshGeometry.rotateZ(8)
  scene.add(meshGeometry)

  return meshGeometry
}

/**
 *
 * @param {number} x Position in x?= -1
 * @param {number} y Position in y?= -3
 * @param {number} z Position in z?= 5
 * @returns Three icosahedron geometry
 */

const createTorusKnotGeometry = (x = -1, y = -3, z = 6) => {
  const geometry2 = new THREE.TorusKnotGeometry(11, 3, 158, 8, 6, 3)
  const material = new THREE.MeshMatcapMaterial()
  material.matcap = matcapTexture

  const sizeShape = 0.08

  const meshGeometry = new THREE.Mesh(geometry2, material)
  meshGeometry.position.x = x
  meshGeometry.position.y = y
  meshGeometry.position.z = z
  meshGeometry.rotateZ(8)
  scene.add(meshGeometry)

  return meshGeometry
}

/**
 * =============================================
 *
 *  SHAPES
 *
 * =============================================
 */

const octahedronGeometry = new THREE.OctahedronGeometry(0.3)
const octahedronMaterial = new THREE.MeshMatcapMaterial()
octahedronMaterial.matcap = matcapTexture

const customPosition = 0.4
for (let i = 0; i < 60; i++) {
  const octahedron = new THREE.Mesh(octahedronGeometry, octahedronMaterial)
  octahedron.position.x = (Math.random() - customPosition) * 15
  octahedron.position.y = (Math.random() - customPosition) * 15
  octahedron.position.z = (Math.random() - customPosition) * 15
  scene.add(octahedron)
}

const geometry1 = createTorusTriangle()
const geometry2 = createIcosahedronGeometry(-1, -3, -6)
const geometry3 = createTorusKnotGeometry()

/**
 * =============================================
 *
 *  ADD EVENT LISTENERS
 *
 * -> Window for resize.
 * -> Scroll on main tag.
 *
 * =============================================
 */

if (window.matchMedia('(min-width: 700px)').matches) {
  const mainTag = document.getElementById('main')

  const moveCamera = () => {
    const top = mainTag.scrollTop
    camera.position.y = top * -0.001
    camera.position.z = top * 0.001
    geometry1.rotateX(top * 0.00002)
    geometry2.rotateY(top * 0.00002)
    geometry3.rotateZ(top * 0.000025)
  }
  mainTag.onscroll = moveCamera
}

window.addEventListener('resize', () => {
  // Save sizes
  sizes.width = window.innerWidth
  sizes.height = window.innerHeight

  // Update camera
  camera.aspect = sizes.width / sizes.height
  camera.updateProjectionMatrix()

  // Update renderer
  renderer.setSize(sizes.width, sizes.height)
})

/**
 * =============================================
 *
 *  LOOP
 *
 * =============================================
 */

const loop = () => {
  renderer.render(scene, camera)
  window.requestAnimationFrame(loop)
}

loop()

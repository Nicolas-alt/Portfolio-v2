const buttonOpenModal = document.getElementById('button__talk')
const buttonCloseModal = document.getElementById('buttonClose')
const form = document.getElementById('formId')
const modal = document.getElementById('section__modal')

const openModal = () => modal.classList.add('section__modal--active')
const closeModal = () => modal.classList.remove('section__modal--active')
const resetForm = () => form.reset()

buttonOpenModal.addEventListener('click', openModal)
buttonCloseModal.addEventListener('click', closeModal)
form.addEventListener('onsubmit', resetForm)

import gsap from 'gsap'
import './threeConfig/customThree'
import './swiperConfig/swiperConfig'
import './message/customMessage'
import './modal/customModal'
import './click/click'

gsap.from('.div__homeText', {
  opacity: 0,
  duration: 5,
  y: -40,
  ease: 'elastic(1, 0.2)'
})

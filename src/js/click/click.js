const click = () => {
  let element = document.createElement('div')
  element.setAttribute('class', 'dot')
  document.body.appendChild(element)

  element.style.top = event.pageY + 'px'
  element.style.left = event.pageX + 'px'

  setTimeout(() => {
    document.body.removeChild(element)
  }, 1000)
}

document.addEventListener('click', click)
